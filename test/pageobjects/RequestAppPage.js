const globals = require('../globals');

class RequestAppPage {
  get sendRequestButton() {return $('button=Send request')}
  get requestSentSuccessfullyFlag() {return './/span[contains(.,"Request sent successfully")]'}
}

module.exports = new RequestAppPage();