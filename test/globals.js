const app_key = 'refapp';
const request_refapp_key = 'request-refapp';
const kitchen_sink_iframe_key = 'kitchen-sink';
const request_app_iframe_key = 'request-refapp-general-page';
const dialog_key = 'basic-dialog-module';
const inline_dialog_key = 'inline-dialog-key';
const kitchen_sink_refapp_selector = `.//iframe[contains(@id,"${app_key}__${kitchen_sink_iframe_key}")]`;
const request_refapp_selector = `.//iframe[contains(@id,"${request_refapp_key}__${request_app_iframe_key}")]`;
const dialog_selector = `.//iframe[contains(@id,"${app_key}__${dialog_key}")]`;
const inline_dialog_selector = `.//iframe[contains(@id,"${app_key}__${inline_dialog_key}")]`

module.exports = {
  app_key,
  kitchen_sink_iframe_key,
  request_app_iframe_key,
  dialog_key,
  inline_dialog_key,
  kitchen_sink_refapp_selector,
  request_refapp_selector,
  dialog_selector,
  inline_dialog_selector
};
