
# We used to use push state URLs which required the creation of directories corresponding
# to each static path in which we'd have to copy the root index.js to handle routing. We
# now use hash based routing which prevents the need for this.


# ACJS-883:
# Having said that, in order to introduce the hash based URL scheme in a backward compatible
# manner, the directories will still be created for a period to prevent app installations
# in real products breaking.

mkdir -p build/app/refapp-app-home
cp build/index.html build/app/refapp-app-home

mkdir -p build/app/hello-world-app
cp build/index.html build/app/hello-world-app

mkdir -p build/app/request-app
cp build/index.html build/app/request-app
