import React, { PureComponent } from 'react';
import ReactDOM from "react-dom";
import MainRouter from "./modules/MainRouter.jsx";
import { Provider } from 'react-redux';
import Store from './store';
import { settings } from './constants';
import { loadSettings, saveSettings } from './localStorage';
import { AtlasKitThemeProvider } from '@atlaskit/theme';
import ConnectModule from "./modules/ConnectModule";

const persistedSettings = loadSettings();
const StoreInstance = Store({ settings: persistedSettings || settings });

ReactDOM.render(<ConnectModule store={StoreInstance} />, document.getElementById('connect-root'));

export default class App extends PureComponent {
  state = {
    productTheme: StoreInstance.getState().settings.productTheme
  };

  constructor() {
    super();
    StoreInstance.subscribe(() => {
      const state = StoreInstance.getState();
      this.setState({
        productTheme: state.settings.productTheme
      });
      saveSettings(state.settings);
    });

  }

  render() {
    return (
      <Provider store={StoreInstance}>
        <AtlasKitThemeProvider mode={this.state.productTheme}>
          <MainRouter store={StoreInstance} />
        </AtlasKitThemeProvider>
      </Provider>
    );
  }
};
