import React, { PureComponent } from 'react';
import ErrorIcon from '@atlaskit/icon/glyph/error';
import { akColorR300 } from '@atlaskit/util-shared-styles';

/**
 * This component renders a red error icon.
 */
export default class RedErrorIcon extends PureComponent {
  render() {
    return (
      <div style={{ color: akColorR300 }}>
        <ErrorIcon label="Error" />
      </div>
    );
  }
}
