import React, { PureComponent } from 'react';
import HintIcon from '@atlaskit/icon/glyph/info';
import { akColorB200 } from '@atlaskit/util-shared-styles';

/**
 * This component renders a blue hint icon.
 */
export default class BlueHintIcon extends PureComponent {
  render() {
    return (
      <div style={{ color: akColorB200 }}>
        <HintIcon label="Hint" />
      </div>
    );
  }
}
