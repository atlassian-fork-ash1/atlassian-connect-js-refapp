import { PureComponent } from 'react';
import PubSub from "../../providers/PubSub";

export default class ConnectView extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {};
    this.onProviderStateChange = this.onProviderStateChange.bind(this);
    PubSub.subscribe(this.onProviderStateChange);
  }

  onProviderStateChange = (providerStateChangeData) => {
    console.log('handling provider state change', providerStateChangeData);
    this.setState(providerStateChangeData.detail.newState);
  };

}
