import React, { PureComponent } from 'react';
import Flag, { FlagGroup } from '@atlaskit/flag';
import '@atlaskit/css-reset';
import GreyGenericIcon from '../acjs/flagIcons/GreyGenericIcon';
import BlueHintIcon from '../acjs/flagIcons/BlueHintIcon';
import PurpleInfoIcon from '../acjs/flagIcons/PurpleInfoIcon';
import GreenSuccessIcon from '../acjs/flagIcons/GreenSuccessIcon';
import YellowWarningIcon from '../acjs/flagIcons/YellowWarningIcon';
import RedErrorIcon from '../acjs/flagIcons/RedErrorIcon';

export default class MessageView extends PureComponent {
  render() {
    return (
      <FlagGroup onDismissed={(messageId) => this.props.actions.dismissMessage(messageId)}>
        {
          this.props.messages.map(message => (
            <Flag
              id={message.id}
              key={message.id}
              title={message.title}
              description={message.body}
              actions={message.actions}
              shouldDismiss={true}
              icon={message.type === 'generic' ? <GreyGenericIcon/> :
                message.type === 'hint' ? <BlueHintIcon/> :
                message.type === 'info' ? <PurpleInfoIcon/> :
                message.type === 'success' ? <GreenSuccessIcon/> :
                message.type === 'warning' ? <YellowWarningIcon/> :
                message.type === 'error' ? <RedErrorIcon/> :
                <PurpleInfoIcon/>
              }
            />
          ))
        }
      </FlagGroup>
    );
  }
}

