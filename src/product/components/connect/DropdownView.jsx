import React from 'react';
import SingleSelect from '@atlaskit/single-select'
import ConnectView from "./ConnectView";

export default class DropdownView extends ConnectView {

  constructor(props) {
    super(props);
    this.dropdownProvider = props.dropdownProvider;
  }

  onItemActivated(e) {
    this.props.activeDropdown.state.itemNotificationCallback({
      dropdownId: this.props.activeDropdown.state.dropdownId,
      item: {
        id: e.item.value,
        content: e.item.content
      }
    });
  }

  render() {
    const activeDropdown = this.props.activeDropdown;
    if (!activeDropdown) {
      console.log('There is no active dropdown.');
      return (<div />);
    }
    if (!activeDropdown.isVisible()) {
      console.log('Active dropdown is not visible.');
      return (<div />);
    }
    console.log('Active dropdown is visible.');
    const containerStyle = {
      zIndex: 1000,
      position: 'absolute',
      width: activeDropdown.state.width,
      top: activeDropdown.state.y + window.pageYOffset,
      left: activeDropdown.state.x + window.pageXOffset
    };
    console.log('activeDropdown: ', activeDropdown);
    let count = 0;
    activeDropdown.state.dropdownGroups.forEach((dropdownGroup) => {
      dropdownGroup.items.forEach((dropdownItem) => {
        dropdownItem.isDisabled = dropdownItem.disabled;
        count++;
      });
    });
    console.log('Active dropdown has ' + count + ' items.');

    return (
      <div style={containerStyle}>
        <SingleSelect
          isOpen={activeDropdown.isVisible()}
          defaultOpen={activeDropdown.isVisible()}
          onSelected={this.onItemActivated.bind(this)}
          shouldFitContainer={true}
          items={activeDropdown.state.dropdownGroups}
         />
      </div>
    );
  }
}
