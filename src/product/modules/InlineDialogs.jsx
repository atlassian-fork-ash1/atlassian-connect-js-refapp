import React, { PureComponent } from 'react';
import '@atlaskit/css-reset';
import connectHost from "atlassian-connect-js";
import AkInlineDialog from '@atlaskit/inline-dialog';
import {AkNavigationItem} from '@atlaskit/navigation';
import { ConnectIframe } from '@atlassian/connect-module-core';
import AppIcon from '@atlaskit/icon/glyph/addon';
import InlineDialogWrapper from '../components/InlineDialogWrapper';
import DefaultConnectIframeProvider from './DefaultConnectIframeProvider';
import IframeFailedIndicator from '../components/IframeFailedIndicator';
import IframeLoadingIndicator from '../components/IframeLoadingIndicator';
import IframeTimeoutIndicator from '../components/IframeTimeoutIndicator';
import RefappUrlBuilder from '../../common/RefappUrlBuilder';

export default class InlineDialogs extends PureComponent {

  constructor() {
    super();
    this.state = {
      openInlineDialogKey: null
    };
  };

  hideInlineDialog = (key) => {
    console.log('hideInlineDialog:', key, '|| current state:', this.state);
    this.setState({openInlineDialogKey: null});
  };

  isInlineDialogOpen = (key) => {
    console.log('isInlineDialogOpen:', key, '|| current state:', this.state);
    return this.state.openInlineDialogKey === key;
  };

  onClickInlineDialogLink = (key) => {
    console.log('onClickInlineDialogLink:', key, '|| current state:', this.state);
    if (this.state.openInlineDialogKey === key) {
      this.setState({openInlineDialogKey: null});
    } else {
      this.setState({openInlineDialogKey: key});
    }
  };

  render() {
    const inlineDialogAppDataOptions =
      'resize:' + this.props.settings.resize +
      ';sizeToParent:' + this.props.settings.sizeToParent +
      ';margin:' + this.props.settings.margin +
      ';base:' + this.props.settings.base;
    const inlineDialogAddonOptions = {
      'autoresize': this.props.settings.autoresize,
      'widthinpx': this.props.settings.widthinpx,
      'hostFrameOffset': 1
    };
    const timeoutIframeUrl = 'https://some-url-that-does-not-exist.com';
    const baseUrl = this.props.settings.timeoutAppIframes ? timeoutIframeUrl : window.location.origin;
    const inlineDialogLinks = [{
      key: 'full',
      linkTitle: 'Full inline dialog',
      url: new RefappUrlBuilder()
        .setBaseUrl(baseUrl)
        .setQueryString('data-options=' + inlineDialogAppDataOptions + '&theme=' + this.props.settings.appTheme)
        .setModuleUrl('/#/app/refapp-app-home/')
        .build()
    }, {
      key: 'minimal',
      linkTitle: 'Minimal inline dialog',
      url: new RefappUrlBuilder()
        .setBaseUrl(baseUrl)
        .setQueryString('data-options=' + inlineDialogAppDataOptions + '&soloPage=InlineDialogPage&theme=' + this.props.settings.appTheme)
        .setModuleUrl('/#/app/refapp-app-home/')
        .build()
    }, {
      key: 'timeout',
      linkTitle: 'Timeout inline dialog',
      url: new RefappUrlBuilder()
        .setBaseUrl(timeoutIframeUrl)
        .setQueryString('data-options=' + inlineDialogAppDataOptions + '&soloPage=InlineDialogPage')
        .setModuleUrl('/#/app/refapp-app-home/')
        .build()
    }];
    return (
      <span>
        {inlineDialogLinks.map((inlineDialogLink) => {
          const handleHideInlineDialog = () => {
            this.hideInlineDialog(inlineDialogLink.key)
          };
          const connectIframeProvider = new DefaultConnectIframeProvider(handleHideInlineDialog);
          return (
            <AkInlineDialog
              key={inlineDialogLink.key}
              content={
                <InlineDialogWrapper>
                  <ConnectIframe
                    connectHost={connectHost}
                    appKey='refapp'
                    moduleKey='inline-dialog-key'
                    failedToLoadIndicator={IframeFailedIndicator}
                    loadingIndicator={IframeLoadingIndicator}
                    timeoutIndicator={IframeTimeoutIndicator}
                    url={inlineDialogLink.url}
                    options={inlineDialogAddonOptions}
                    onHideInlineDialog={() => this.hideInlineDialog(inlineDialogLink.key)}
                    connectIframeProvider={connectIframeProvider}
                  />
                </InlineDialogWrapper>
              }
              position={'right middle'}
              isOpen={this.isInlineDialogOpen(inlineDialogLink.key)}
              onClose={() => this.hideInlineDialog(inlineDialogLink.key)}
            >
              <AkNavigationItem
                icon={<AppIcon label={inlineDialogLink.linkTitle} />}
                text={inlineDialogLink.linkTitle}
                onClick={() => this.onClickInlineDialogLink(inlineDialogLink.key)}
              />
            </AkInlineDialog>
          );
        })}
      </span>
    )
  }
}
