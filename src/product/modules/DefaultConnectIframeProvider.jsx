import {ConnectIframeProvider} from '@atlassian/connect-module-core';
import { storeNestedIframeJSON } from '../../common/ProductAPIMock';

export default class DefaultConnectIframeProvider implements ConnectIframeProvider {

  constructor(handleHideInlineDialog) {
    this.handleHideInlineDialog = handleHideInlineDialog;
  }

  onStoreNestedIframeJSON(connectIframeProps) {
    storeNestedIframeJSON(connectIframeProps);
  }

  onHideInlineDialog() {
    this.handleHideInlineDialog();
  }

  getLoadingTimeoutMilliseconds(appKey) {
    // In the ACJS we use an aggressive timeout value so we can test more quickly.
    return 2000;
  }

  handleIframeLoadingStarted(appKey) {
    console.log('DefaultConnectIframeProvider.handleIframeLoadingStarted', appKey);
  }

  handleIframeLoadingComplete(appKey) {
    console.log('DefaultConnectIframeProvider.handleIframeLoadingComplete', appKey);
  }

  handleIframeUnload(appKey) {
    console.log('DefaultConnectIframeProvider.handleIframeUnload', appKey);
  }

  handleIframeLoadTimeout(appKey, cancelLoadingCallback) {
    console.log('DefaultConnectIframeProvider.handleIframeLoadTimeout', appKey);
  }

  _isJwtExpired(url, connectHost) {
    return (url && (connectHost.hasJwt(url) && connectHost.isJwtExpired(url)));
  }

  resolveIframeContext = (iframeContext, connectHost) => {
    return new Promise(resolve => {

      if (!this._isJwtExpired(iframeContext.url, connectHost)) {
        resolve(iframeContext);
        return;
      }

      if (!connectHost.getContentResolver())
      {
        console.warn('No content resolver supplied');
        resolve(iframeContext);
        return;
      }

      connectHost.getContentResolver()({
        addon_key: iframeContext.appKey,
        key: iframeContext.moduleKey,
        options: iframeContext.options,
        classifier: 'json'
      })
      .then(promiseData => {
        if (typeof promiseData !== 'object' || !promiseData.hasOwnProperty('url')) {
          console.log('Invalid response');
          resolve(iframeContext);
          return;
        } else {
          resolve({
            url: promiseData.url,
            appKey: promiseData.addon_key || iframeContext.appKey,
            moduleKey: promiseData.key || iframeContext.moduleKey,
            options: Object.assign({}, iframeContext.options, promiseData.options)
          });
          return;
        }
      })
      .catch(console.warn);
    });
  }
}
