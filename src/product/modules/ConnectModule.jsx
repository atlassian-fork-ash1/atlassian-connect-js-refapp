import React, { PureComponent } from 'react';

import { DialogModule } from '@atlassian/connect-module-core';
import { DropdownModule } from '@atlassian/connect-module-core';
import { FlagModule } from '@atlassian/connect-module-core';
import { InlineDialogModule } from '@atlassian/connect-module-core';
import { MessageModule } from '@atlassian/connect-module-core';

import { UPDATE_ATLASKIT } from '../actions/settings';

import connectProviders from '../providers/ConnectProviders'
import connectHost from 'atlassian-connect-js';

/**
 * This component is responsible for mounting the Connect modules required by the product.
 */
export default class ConnectModule extends PureComponent {

  moduleNamesToMetadata = {
    'flag': {
      allowSwitchToAui: true
    }, 'dialog': {
      allowSwitchToAui: true
    }, 'dropdown': {
      allowSwitchToAui: true
    }, 'inlineDialog': {
      allowSwitchToAui: false
    }, 'messages': {
      allowSwitchToAui: true
    }
  };

  constructor(props) {
    super();
    this.dialogProvider = connectProviders.dialogProvider;
    this.dropdownProvider = connectProviders.dropdownProvider;
    this.flagProvider = connectProviders.flagProvider;
    this.inlineDialogProvider = connectProviders.inlineDialogProvider;
    this.messageProvider = connectProviders.messageProvider;
    this.appProvider = connectProviders.appProvider;
    connectHost.onFrameClick(iframe => {
      // clicks inside an iframe don't register in the parent window
      // this causes popups like inline dialog not to behave as expected
      // triggering the click on the iframe element fixes this
      iframe.click();
    });
  };

  componentDidMount = () => {
    this.moduleNamesToModules = {};
    for (let moduleName in this.moduleNamesToMetadata) {
      if (this.moduleNamesToMetadata.hasOwnProperty(moduleName)) {
        this.moduleNamesToModules[moduleName] = connectProviders.adaptor.getModuleByName(moduleName);
      }
    }
    const store = this.props.store;
    store.subscribe(() => {
      const storeState = this.props.store.getState();
      const settings = storeState.settings;
      for (let moduleName in this.moduleNamesToMetadata) {
        if (this.moduleNamesToMetadata.hasOwnProperty(moduleName)) {
          const metadata = this.moduleNamesToMetadata[moduleName];
          if (metadata.allowSwitchToAui) {
            const module = this.moduleNamesToModules[moduleName];
            const atlasKitEnabled = (settings[UPDATE_ATLASKIT].find(item => item.value === moduleName) !== undefined);
            module.setEnabled(atlasKitEnabled);
          }
        }
      }

    });
  }

  render() {
    return (
      <div>
        <DialogModule
          adaptor={connectProviders.adaptor}
          dialogProvider={connectProviders.dialogProvider}
        />
        <DropdownModule
          adaptor={connectProviders.adaptor}
          dropdownProvider={connectProviders.dropdownProvider}
        />
        <FlagModule
          adaptor={connectProviders.adaptor}
          flagProvider={connectProviders.flagProvider}
        />
        <InlineDialogModule
          adaptor={connectProviders.adaptor}
          inlineDialogProvider={connectProviders.inlineDialogProvider}
        />
        <MessageModule
          adaptor={connectProviders.adaptor}
          messageProvider={connectProviders.messageProvider}
        />
      </div>
    );
  }
}

