import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import '@atlaskit/css-reset';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import connectHost from 'atlassian-connect-js';
import connectProviders from '../providers/ConnectProviders'

import { AtlasKitDialogView } from '../providers/AtlasKitDialogProvider';
import { AtlasKitDropdownView } from '../providers/AtlasKitDropdownProvider';
import { AtlasKitFlagView } from '../providers/AtlasKitFlagProvider';
import { AtlasKitMessageView } from '../providers/AtlasKitMessageProvider';
import AppProvider from '../providers/AppProvider';
import AppNavLinks from '../appmanagement/AppNavLinks';
import * as flagActions from '../actions/flag';

import Page from '@atlaskit/page';
import Navigation, { AkNavigationItemGroup, AkNavigationItem, AkContainerTitle, presetThemes } from '@atlaskit/navigation';

import DashboardIcon from '@atlaskit/icon/glyph/dashboard';
import GearIcon from '@atlaskit/icon/glyph/settings';
import JiraIcon from '@atlaskit/icon/glyph/jira';
import AtlassianIcon from '@atlaskit/icon/glyph/atlassian';
import SearchIcon from '@atlaskit/icon/glyph/search';
import CreateIcon from '@atlaskit/icon/glyph/add';
import ArrowleftIcon from '@atlaskit/icon/glyph/arrow-left';

import CreateDrawer from '../components/CreateDrawer';
import SearchDrawer from '../components/SearchDrawer';
import HelpDropdownMenu from '../components/HelpDropdownMenu';
import AccountDropdownMenu from '../components/AccountDropdownMenu';

import RefappUtil from '../../common/RefappUtil';
import LayerManager from '@atlaskit/layer-manager';

class ProductApp extends PureComponent {

  constructor(props) {
    super();
    this.state = {
      isCreateDrawerOpen: false,
      isSearchDrawerOpen: false,
      currentLocation: window.location
    };
    connectHost.registerProvider('addon', AppProvider);
    connectHost.registerContentResolver.resolveByExtension(RefappUtil.getContentResolver(props.settings));
    props.history.listen(e =>  {
      this.setState({currentLocation: e.pathname});
    });
  };

  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object,
  };

  static propTypes = {
    navOpenState: PropTypes.object,
    onNavResize: PropTypes.func,
  };

  static childContextTypes = {
    showModal: PropTypes.func,
    addFlag: PropTypes.func,
  };

  getChildContext() {
    return {
      showModal: this.showModal,
      addFlag: this.addFlag,
    };
  }

  render() {
    return (
      <LayerManager>
        <div>
          <div id="ac-message-container" style={{position:'absolute', width: '500px', top: '10px', right: '10px', zIndex: '100'}}>
          </div>
          <Page
            navigationWidth={this.context.navOpenState.width}
            navigation={
              <Navigation
                containerTheme={presetThemes[this.props.settings.productTheme]}
                globalTheme={presetThemes[this.props.settings.productTheme]}
                isOpen={this.context.navOpenState.isOpen}
                width={this.context.navOpenState.width}
                onResize={this.props.onNavResize}
                containerHeaderComponent={() => (
                  <AkContainerTitle
                    href="#foo"
                    icon={<img alt="nucleus" src="/ACJS.png" />}
                    text="ACJS Refapp"
                  />
                )}
                globalPrimaryIcon={<AtlassianIcon label="Atlassian icon" size="medium" />}
                globalSearchIcon={<SearchIcon label="Search icon" />}
                hasBlanket
                drawerBackIcon={<ArrowleftIcon label="Back icon" size="medium" />}
                globalAccountItem={AccountDropdownMenu}
                globalCreateIcon={<CreateIcon label="Create icon" />}
                globalHelpItem={HelpDropdownMenu}
                isSearchDrawerOpen={this.state.isSearchDrawerOpen}
                onSearchDrawerOpen={() => (this.setState({ isSearchDrawerOpen: true }))}
                onSearchDrawerClose={() => (this.setState({ isSearchDrawerOpen: false }))}
                searchDrawerContent={
                  <SearchDrawer
                    onResultClicked={() => this.setState({ isSearchDrawerOpen: false })}
                    onSearchInputRef={(ref) => {
                      this.searchInputRef = ref;
                    }}
                  />
                }
                isCreateDrawerOpen={this.state.isCreateDrawerOpen}
                onCreateDrawerOpen={() => (this.setState({ isCreateDrawerOpen: true }))}
                onCreateDrawerClose={() => (this.setState({ isCreateDrawerOpen: false }))}
                createDrawerContent={
                  <CreateDrawer
                    onItemClicked={() => this.setState({ isCreateDrawerOpen: false })}
                  />
                }
              >
                <AkNavigationItemGroup
                  key="Reference host"
                  title="Reference host"
                >
                  <Link key='/' to='/'>
                    <AkNavigationItem
                      icon={<DashboardIcon label='Host Home' />}
                      text='Host Home'
                      isSelected={this.context.router.route.location.pathname === '/'}
                    />
                  </Link>
                  <Link key='/settings' to='/settings'>
                    <AkNavigationItem
                      icon={<GearIcon label='Settings' />}
                      text='Settings'
                      isSelected={this.context.router.route.location.pathname === '/settings'}
                    />
                  </Link>
                </AkNavigationItemGroup>

                <AkNavigationItemGroup
                  key="Mock hosts"
                  title="Mock hosts"
                >
                  <Link key='/jira' to='/jira'>
                    <AkNavigationItem
                      icon={<JiraIcon label='Jira' />}
                      text='Jira'
                      isSelected={this.context.router.route.location.pathname === '/jira'}
                    />
                  </Link>
                </AkNavigationItemGroup>

                <AkNavigationItemGroup
                  key="General Pages"
                  title="General Pages"
                >
                  <AppNavLinks
                    moduleType="generalPages"
                    showKitchenSinkInlineDialogs={false}
                    currentLocation={this.state.currentLocation}
                    router={this.context.router}
                    settings={this.props.settings}
                    actions={this.props.actions}
                  />
                </AkNavigationItemGroup>

                <AkNavigationItemGroup
                  key="Dynamic Content Macros"
                  title="Dynamic Content Macros"
                >
                  <AppNavLinks
                    moduleType="dynamicContentMacros"
                    showKitchenSinkInlineDialogs={false}
                    currentLocation={this.state.currentLocation}
                    router={this.context.router}
                    settings={this.props.settings}
                    actions={this.props.actions}
                  />
                </AkNavigationItemGroup>

                <AkNavigationItemGroup
                  key="Kitchen Sink Inline Dialogs"
                  title="Kitchen Sink Inline Dialogs"
                >
                  <AppNavLinks
                    moduleType={undefined}
                    showKitchenSinkInlineDialogs={true}
                    currentLocation={this.state.currentLocation}
                    router={this.context.router}
                    settings={this.props.settings}
                    actions={this.props.actions}
                  />
                </AkNavigationItemGroup>
              </Navigation>
            }
          >
            {this.props.children}
          </Page>
          <div>
            <AtlasKitDialogView
              dialogProvider={connectProviders.dialogProvider}
              settings={this.props.settings}
              connectHost={connectHost}
            />
            <AtlasKitFlagView
              flagProvider={connectProviders.flagProvider}
            />
            <AtlasKitDropdownView
                dropdownProvider={connectProviders.dropdownProvider}
            />
          </div>
          <div>
            <AtlasKitMessageView
              messageProvider={connectProviders.messageProvider}
            />
          </div>
        </div>
      </LayerManager>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    settings: state.settings
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      ...flagActions
    }, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductApp);
