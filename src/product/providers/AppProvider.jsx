const AppProvider = {
  resize: (width, height, context) => {
    context.extension.options.resize(width, height);
  },

  sizeToParent: (hideFooter, context) => {
    context.extension.options.sizeToParent(hideFooter);
  },

  registerUnmountCallback: (callback, context) => {
    context.extension.options.registerUnmountCallback(callback);
  },

};

export default AppProvider;