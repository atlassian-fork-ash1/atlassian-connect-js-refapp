import { DefaultInlineDialogProvider } from '@atlassian/connect-module-core';
import { InlineDialogProviderValidator } from '@atlassian/connect-module-core';

/**
 * This test validates our implementation of the InlineDialogProvider. The validation logic is owned
 * by connect-module-core.
 */
describe('InlineDialogProviderValidationTesting', () => {

  it('Should implement the InlineDialogProvider API properly.', () => {
    // Actually connect-module-core provides a default implementation of InlineDialogProvider which
    // we are validating here so this test isn't really adding much value. If, however, we used a
    // different implementation then validating it here would add value.
    const inlineDialogProvider = new DefaultInlineDialogProvider();
    InlineDialogProviderValidator.validate(inlineDialogProvider);
  });

});