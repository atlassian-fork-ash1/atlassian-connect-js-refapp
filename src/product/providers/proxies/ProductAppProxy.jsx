
export default class ProductAppProxy {

  constructor(identifier) {
    console.log('ProductAppProxy.constructor:', identifier);
    this.state = {
      identifier: identifier
    };
  }

  getIdentifier() {
    console.log('ProductAppProxy.getIdentifier:');
    return this.state.identifier;
  }

  setHeight(height) {

  }

}
