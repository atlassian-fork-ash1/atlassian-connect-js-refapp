import { CREATE_FLAG, CLOSE_FLAG, DISMISS_FLAG } from '../actions/flag';

export default (state = [], payload) => {
  switch (payload.type) {
    case CREATE_FLAG:
      return [...state, payload.options];
    case CLOSE_FLAG:
      return [...state].map(flag => {
        if (flag.id === payload.id) {
          flag.shouldDismiss = true;
        }
        return flag;
      });
    case DISMISS_FLAG:
      return [...state].filter(flag => {
        let isTarget = flag.id === payload.id;
        if (isTarget) {
          flag.onClose(flag.id);
        }
        return !isTarget;
      });
    default:
      return state;
  }
};
