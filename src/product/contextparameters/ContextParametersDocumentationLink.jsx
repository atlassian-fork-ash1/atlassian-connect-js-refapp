import React, {PureComponent} from "react";
import ExternalLink from '../components/ExternalLink';

export default class ContextParametersDocumentationLink extends PureComponent {

  render() {
    const contextParametersUrl = 'https://developer.atlassian.com/cloud/confluence/context-parameters/';
    return (
      <ExternalLink
        href={contextParametersUrl}
        label="Context Parameters"
      >
        {this.props.children ? this.props.children : 'Context Parameters'}
      </ExternalLink>
    );
  }

}
