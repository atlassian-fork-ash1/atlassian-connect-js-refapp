import React, { PureComponent } from 'react';
import DynamicTable from '@atlaskit/dynamic-table';
import Button from '@atlaskit/button';
import EditIcon from '@atlaskit/icon/glyph/edit';
import Modal from "@atlaskit/modal-dialog";
import ContextParametersDAO from "./ContextParametersDAO";
import ContextParameterEditor from './ContextParameterEditor';
import ContextParametersDocumentationLink from '../contextparameters/ContextParametersDocumentationLink';
import RefreshIcon from '@atlaskit/icon/glyph/refresh';

export default class ProductMockingSettings extends PureComponent {

  state = {
    selectedItemToEdit: null,
    ContextParameters: ContextParametersDAO.getItems()
  };

  onItemChange = (item) => {
    ContextParametersDAO.updateItem(item);
  };

  onEditItem = (item) => {
    this.setState({selectedItemToEdit: item});
  };

  onResetMockingSettings = (event) => {
    const items = ContextParametersDAO.resetToDefault();
    this.setState({ ContextParameters: items });
    this.propogateSettingsChange(event);
  };

  saveAndCloseItemEditor = () => {
    ContextParametersDAO.updateItem(this.state.selectedItemToEdit)
    this.setState({selectedItemToEdit: null});
  };

  renderOperationsCell = (item) => {
    return (
      <div>
        <Button
          appearance='default'
          iconBefore={<EditIcon label='' />}
          target='_blank'
          onClick={(clickEvent) => this.onEditItem(item)}
        >
          Edit
        </Button>
      </div>
    );
  };

  render = () => {
    const caption = undefined;
    const head = {
      cells: [{
        key: 'key',
        content: <span>Key</span>,
        isSortable: false
      }, {
        key: 'value',
        content: <span>Value</span>,
        isSortable: false
      }, {
        key: 'actions',
        content: <span>Actions</span>,
        isSortable: false
      }]
    };
    const rows = [];
    for (let i = 0; i < this.state.ContextParameters.length; i++) {
      const item = this.state.ContextParameters[i];
      const rowNum = rows.length + 1;
      const row = {
        cells: [{
          key: 'key',
          content: <span>{item.key}</span>
        }, {
          key: 'value',
          content: <span>{item.value}</span>
        }, {
          key: 'actions',
          content: this.renderOperationsCell(item)
        }], key: 'row-' + rowNum
      };
      rows.push(row);
    }
    const actions = [
      { text: 'Save and close', onClick: this.saveAndCloseItemEditor }
    ];
    return (
      <div>
        <p>
          Reference documentation: <ContextParametersDocumentationLink />
        </p>
        <DynamicTable
          caption={caption}
          head={head}
          rows={rows}
          rowsPerPage={40}
          defaultPage={1}
          loadingSpinnerSize="large"
          isLoading={false}
          isFixedSize
          sortKey={this.state.sortKey}
          defaultSortKey={this.state.sortKey}
          defaultSortOrder="DESC"
          onSort={() => {}}
          onSetPage={() => {}}
        />

        <Button
          appearance='warning'
          iconBefore={<RefreshIcon label='' />}
          target='_blank'
          onClick={this.onResetMockingSettings}
        >
          Reset context parameters
        </Button>

        {this.state.selectedItemToEdit && (
          <Modal
            actions={actions}
            onClose={this.saveAndCloseItemEditor}
            heading={this.state.selectedItemToEdit.key + ' Editor'}
            width="x-large"
            shouldCloseOnOverlayClick={false}
          >
            <ContextParameterEditor
              item={this.state.selectedItemToEdit}
              onItemChange={this.onItemChange}
            />
          </Modal>
        )}
      </div>
    );
  };

}
