import React, { PureComponent } from 'react';
import Avatar, { AvatarItem } from "@atlaskit/avatar";
import Lozenge from "@atlaskit/lozenge";
import styled from "styled-components";

export default class JiraIssueStatus extends PureComponent {

  state = {
  };

  render() {
    const Label = styled.div`
      color: #777;
    `;
    const Value = styled.div`
      color: #000;
    `;
    return (
      <div>
        <h5>Details:</h5>
        <table
          style={{borderCollapse: "separate"}}
        >
          <tbody>
            <tr>
              <td>
                <Label>Assignee:</Label>
              </td>
            </tr>
            <tr>
              <td>
                <Value>
                  <AvatarItem
                    avatar={<Avatar src={this.props.issue.assignee.avatar} presence="online" />}
                    key={this.props.issue.assignee.email}
                    onClick={console.log}
                    primaryText={this.props.issue.assignee.name}
                    secondaryText={this.props.issue.assignee.email}
                  />
                </Value>
              </td>
            </tr>
            <tr>
              <td>
                <Label>Reporter:</Label>
              </td>
            </tr>
            <tr>
              <td>
                <Value>
                  <AvatarItem
                    avatar={<Avatar src={this.props.issue.reporter.avatar} presence="offline" />}
                    key={this.props.issue.reporter.email}
                    onClick={console.log}
                    primaryText={this.props.issue.reporter.name}
                    secondaryText={this.props.issue.reporter.email}
                  />
                </Value>
              </td>
            </tr>
            <tr>
              <td>
                <Label>Status:</Label>
              </td>
            </tr>
            <tr>
              <td>
                <Value>
                  <Lozenge appearance='moved'>{this.props.issue.status}</Lozenge>
                </Value>
              </td>
            </tr>
            <tr>
              <td>
                <Label>Components:</Label>
              </td>
            </tr>
            <tr>
              <td>
                <Value>
                  {
                    this.props.issue.components.map((component) => (
                      <Lozenge key={component}>{component}</Lozenge>
                    ))
                  }
                </Value>
              </td>
            </tr>
            <tr>
              <td>
                <Label>Labels:</Label>
              </td>
            </tr>
            <tr>
              <td>
                <Value>
                  {
                    this.props.issue.labels.map((label) => (
                      <Lozenge key={label}>{label}</Lozenge>
                    ))
                  }
                </Value>
              </td>
            </tr>
            <tr>
              <td>
                <Label>Affects version/s:</Label>
              </td>
            </tr>
            <tr>
              <td>
                <Value>
                  {this.props.issue.affectsVersion}
                </Value>
              </td>
            </tr>
            <tr>
              <td>
                <Label>Fix version/s:</Label>
              </td>
            </tr>
            <tr>
              <td>
                <Value>
                  {this.props.issue.fixVersion}
                </Value>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
