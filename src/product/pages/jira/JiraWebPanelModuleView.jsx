import React, { PureComponent } from 'react';

import ArrowButton from "./ArrowButton";
import { ConnectIframe } from "@atlassian/connect-module-core";
import connectHost from "atlassian-connect-js";
import DefaultConnectIframeProvider from "../../modules/DefaultConnectIframeProvider";
import IframeFailedIndicator from "../../components/IframeFailedIndicator";
import IframeLoadingIndicator from "../../components/IframeLoadingIndicator";
import IframeTimeoutIndicator from "../../components/IframeTimeoutIndicator";
import RefappUrlBuilder from '../../../common/RefappUrlBuilder';

export default class JiraWebPanelModuleView extends PureComponent {

  state = {
  };

  render() {
    const baseUrl = window.location.origin;
    const moduleAppDataOptions =
      'resize:true' +
      ';sizeToParent:false' +
      ';margin:20' +
      ';base:' + baseUrl;
    const moduleAddonOptions = {
      'autoresize': true,
      // 'widthinpx': 300,
      'hostFrameOffset': 1
    };
    const moduleUrl = new RefappUrlBuilder()
      .setBaseUrl(baseUrl)
      .setQueryString('data-options=' + moduleAppDataOptions)
      .setModuleUrl(this.props.module.url)
      .build();
    const handleHideInlineDialog = () => {
      // ignore - won't be called
    };
    const connectIframeProvider = new DefaultConnectIframeProvider(handleHideInlineDialog);
    return (
      <div
        style={{
          height: 'auto',
          overflow: 'auto'
        }}
      >
        <div
          style={{
            borderBottom: '2px solid #ccc',
            paddingBottom: '6px',
            marginBottom: '6px'
          }}
        >
          <ArrowButton
            left={true}
            onArrowClick={(event) => this.props.onExitExtensionView()}
          />
          {
            this.props.module.name.value
          }
        </div>
        <div
          style={{
            height: 'auto',
            overflow: 'auto'
          }}
        >
          <ConnectIframe
            connectHost={connectHost}
            appKey='reference-app-key'
            moduleKey='inline-dialog-key'
            failedToLoadIndicator={IframeFailedIndicator}
            loadingIndicator={IframeLoadingIndicator}
            timeoutIndicator={IframeTimeoutIndicator}
            url={moduleUrl}
            options={moduleAddonOptions}
            connectIframeProvider={connectIframeProvider}
          />
        </div>
      </div>
    );
  }
}
