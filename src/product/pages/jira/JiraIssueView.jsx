import React, { PureComponent } from 'react';
import JiraIssueActivity from "./JiraIssueActivity";
import JiraIssueGlancesListView from "./JiraIssueGlancesListView";
import JiraIssueStatus from "./JiraIssueStatus";
import JiraWebPanelModuleView from "./JiraWebPanelModuleView";
import styled from "styled-components";

export default class JiraIssueView extends PureComponent {

  state = {
    selectedExtension: null
  };

  onSelectExtension = (extension) => {
    this.setState({
      selectedExtension: extension
    });
  }

  onDeselectExtension = () => {
    this.setState({selectedExtension: null});
  }

  render() {
    const LeftContainer = styled.div`
      float: left;
      width: 70%;
      margin: 20px 0px;
      padding: 0px;
    `;
    const RightContainer = styled.div`
      float: left;
      width: 30%;
      margin: 20px 0px;
      padding: 0px;
    `;
    return (
      <div>
        <div>
          <LeftContainer>
            <p>
              {this.props.issue.description}
            </p>
            <h5>Attachments:</h5>
            {
              this.props.issue.attachmentImages.map((attachmentImage) => (
                <img key={attachmentImage.key} src={attachmentImage.image} alt="Attachment" />
              ))
            }
            <JiraIssueActivity issue={this.props.issue} />
          </LeftContainer>
          <RightContainer>
            {
              this.state.selectedExtension ?
                <JiraWebPanelModuleView
                  module={this.state.selectedExtension}
                  onExitExtensionView={this.onDeselectExtension}
                >

                </JiraWebPanelModuleView> :
                (
                  <div>
                    <JiraIssueStatus issue={this.props.issue} />
                    <JiraIssueGlancesListView
                      webPanelModules={this.props.webPanelModules}
                      onSelectExtension={this.onSelectExtension}
                    />
                  </div>
                )
            }
          </RightContainer>
        </div>
      </div>
    );
  }
}
