import donaldAvatarImg from "../../assets/donald-avatar.png";
import kimAvatarImg from "../../assets/kim-avatar.png";
import tableFlipImg from "../../assets/table-flip.png";

export default class MockJiraIssueData {

  static lastCommentIndex = 0;

  static getMockJiraIssue() {
    const issue = {
      key: "DON-1234",
      summary: "Let me tell you",
      attachmentImages: [{key: 'a', image: tableFlipImg}],
      description: "Let me tell you, I’m a really smart guy.",
      status: 'In progress',
      reporter: {name: 'Donald Trump', email: 'donald@whitehouse.gov', avatar: donaldAvatarImg},
      assignee: {name: 'Kim Jong-un', email: 'kim@northkorea.gov.kp', avatar: kimAvatarImg},
      components: ['framework', 'navigation'],
      labels: ['asap'],
      affectsVersion: 'None',
      fixVersion: 'None',
      comments: []
    };
    var commentCount = 5;
    for (var index = 0; index < commentCount; index++) {
      issue.comments.push(MockJiraIssueData.buildComment());
    }
    return issue;
  }

  static buildComment() {
    MockJiraIssueData.lastCommentIndex++;
    const isDonald = MockJiraIssueData.lastCommentIndex % 2 === 0;
    const authorName = isDonald ? 'Donald Trump' : 'Kim Jong-un';
    const text = isDonald ?
      'It\'s freezing and snowing in New York - we need global warming!' :
      'The revolution is carried out by means of one\'s thought.';
    const avatarImg = isDonald ? donaldAvatarImg : kimAvatarImg;
    return {
      key: 'comment-' + MockJiraIssueData.lastCommentIndex,
      authorName: authorName,
      text: text,
      type: 'author',
      restrictions: 'Admins',
      time: '30 August, 2017',
      avatarImg: avatarImg
    };
  }

}
