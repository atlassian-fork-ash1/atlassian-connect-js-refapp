import React, { PureComponent } from 'react';

import ArrowButton from "./ArrowButton";

export default class JiraIssueGlancesListView extends PureComponent {

  state = {
  };

  render() {
    return (
      <div>
        {
          this.props.webPanelModules.map(module => (
            <p
              key={module.key}
            >
              <ArrowButton
                onArrowClick={(event) => this.props.onSelectExtension(module)}
              />
              <span
                style={{
                  height: '32px',
                  lineHeight: '32px',
                  textAlign: 'center',
                }}
              >
                {module.name.value}
              </span>
            </p>
          ))
        }
      </div>
    );
  }
}
