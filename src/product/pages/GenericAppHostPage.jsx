import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import AppDAO from "../appmanagement/AppDAO";
import AddCircleIcon from '@atlaskit/icon/glyph/add-circle';
import { connect } from 'react-redux';
import connectHost from "atlassian-connect-js";
import PageTitle from '../components/PageTitle';
import ContextParametersDocumentationLink from '../contextparameters/ContextParametersDocumentationLink';
import DropdownMenu, {DropdownItem} from '@atlaskit/dropdown-menu';
import ContentWrapper from '../components/ContentWrapper';
import { ConnectIframe } from '@atlassian/connect-module-core';
import { iframeMaxHeight, iframeMaxWidth } from '../constants';
import { storeNestedIframeJSON } from '../../common/ProductAPIMock';
import { appMinWidth, appMinHeight } from '../../common/CommonConstants';
import DefaultConnectIframeProvider from '../modules/DefaultConnectIframeProvider';
import IframeLoadingIndicator from '../components/IframeLoadingIndicator';
import IframeTimeoutIndicator from '../components/IframeTimeoutIndicator';
import IframeFailedIndicator from '../components/IframeFailedIndicator';
import { AppLinkUtil } from '../appmanagement/AppNavLinks';
import { Label } from '@atlaskit/field-base'; //ref: AK-1621
import FieldText from '@atlaskit/field-text';
import { ToggleStateless } from '@atlaskit/toggle';
import Button from '@atlaskit/button';
import RefappUtil from '../../common/RefappUtil';
import ShortcutIcon from '@atlaskit/icon/glyph/shortcut';

class GenericAppHostPage extends PureComponent {
  static propTypes = {
    settings: PropTypes.shape({
      autoresize: PropTypes.bool,
      widthinpx: PropTypes.bool,
      isFullPage: PropTypes.bool,
      resize: PropTypes.bool,
      sizeToParent: PropTypes.bool,
      margin: PropTypes.bool,
      base: PropTypes.bool,
      atlaskit: PropTypes.array,
      productTheme: PropTypes.string,
      appTheme: PropTypes.string
    })
  };

  constructor(props) {
    super(props);
    this.adaptor = connectHost.getFrameworkAdaptor();
    this.state = {
      showAppRenderingControls: false,
      showAppFrameBorder: false,
      parameterIdsToValues: {}
    };
  }

  componentWillUnmount(props) {
    /**
    * i would filter the extension list,
    * but this.determineModule relies on the URL which has already
    * changed by the time this function runs
    */
    const extensions = connectHost.getExtensions({});
    extensions.forEach((extension) => {
      connectHost.destroy(extension.extension_id);
    });
  }

  nestedConnectIframes(n, connectHost, url, options, hostFrameOffset=1) {
    if (n === 0) {
      return null;
    } else {
      const appModulePair = this.determineModule();
      const app = appModulePair.app;
      const module = appModulePair.module;
      const handleHideInlineDialog = () => {
        // Nothing to do here - I'm not in an inline dialog.
      };
      const connectIframeProvider = new DefaultConnectIframeProvider(handleHideInlineDialog);
      hostFrameOffset++;
      return (
        <ConnectIframe
          connectHost={connectHost}
          appKey={app.key}
          moduleKey={module.key}
          failedToLoadIndicator={IframeFailedIndicator}
          loadingIndicator={IframeLoadingIndicator}
          timeoutIndicator={IframeTimeoutIndicator}
          url={url}
          options={Object.assign({}, options, {hostFrameOffset: hostFrameOffset})}
          height={appMinHeight}
          width={appMinWidth}
          storeNestedIframeJSON={storeNestedIframeJSON}
          connectIframeProvider={connectIframeProvider}
        >
          {this.nestedConnectIframes(n-1, connectHost, url, options, hostFrameOffset)}
        </ConnectIframe>
      );
    }
  }

  determineModule = () => {
    const currentHash = window.location.hash;
    const appKeyModuleKey = AppLinkUtil.parseAppLinkFromHash(currentHash);
    let app = null;
    let module = null;
    AppDAO.iterateModules(AppDAO.allAppsFilter, (thisApp, thisModuleType, thisModule) => {
      if (thisApp.key === appKeyModuleKey.appKey && thisModule.key === appKeyModuleKey.key) {
        app = thisApp;
        module = thisModule;
      }
    });
    return {app: app, module: module};
  }

  buildGeneratedModuleUrl = (app, module) => {
    const dataOptions = {};
    const addonKey = app.key;
    const moduleKey = module.key;
    const params = {
      addon_key: addonKey,
      key: moduleKey,
      options: dataOptions,
      classifier: 'json'
    };
    const resolvedModule = RefappUtil.resolveModule(params, this.props.settings);
    if (resolvedModule) {
      return resolvedModule.url;
    } else {
      console.error('Unable to resolve module', params);
      return null;
    }
  }

  getParameterValue = (parameter) => {
    const parameterIdsToValues = this.state.parameterIdsToValues;
    return parameterIdsToValues[parameter.identifier];
  }

  setParameterValue = (parameter, value) => {
    const parameterIdsToValues = this.state.parameterIdsToValues;
    parameterIdsToValues[parameter.identifier] = value;
    this.setState({
      parameterIdsToValues: parameterIdsToValues
    });
    this.forceUpdate();
  }

  getParameterName = (parameter) => {
    return parameter.name && parameter.name.value ? parameter.name.value : '???';
  }

  renderEnumParameter = (parameter) => {
    let parameterValue = this.getParameterValue(parameter);
    const dropDownItems = parameter.values.map((value) => {
      return (
        <DropdownItem
          key={value}
          id={value}
          onClick={() => this.setParameterValue(parameter, value)}
        >
          {value}
        </DropdownItem>
      );
    });
    return (
      <div key={parameter.identifier}>
        <Label label={this.getParameterName(parameter)} />
        <DropdownMenu
          trigger={parameterValue ? parameterValue : 'Select'}
          defaultOpen={false}
          triggerType="button"
          shouldFitContainer={false}
        >
          {dropDownItems}
        </DropdownMenu>
      </div>
    );
  }

  onShowUrlParameterSettings = () => {
    this.setState({showAppRenderingControls: true});
  };

  renderStringParameter = (parameter) => {
    const parameterValue = this.getParameterValue(parameter);
    const fieldValue = parameterValue ? parameterValue : '';
    return (
      <div key={parameter.identifier}>
        <FieldText
          label={this.getParameterName(parameter)}
          value={fieldValue}
          compact={false}
          onChange={(event) => this.setParameterValue(parameter, event.target.value)}
        />
      </div>
    );
  }

  renderUnknownParameterType = (parameter) => {
    return (
      <div key={parameter.identifier}>
        <Label label='Show app iframe border' />
        <span style={{color: '#FF5630'}}>Unknown parameter type: {parameter.type}</span>
      </div>
    );
  }

  renderModuleParameter = (parameter) => {
    if (parameter.type === 'enum') {
      return this.renderEnumParameter(parameter);
    } else if (parameter.type === 'string') {
      return this.renderStringParameter(parameter);
    } else {
      return this.renderUnknownParameterType(parameter);
    }
  }

  renderModuleParameters = (module) => {
    const renderedParameters = module.parameters.map((parameter) => {
      return this.renderModuleParameter(parameter);
    });
    return (
      <div style={{marginTop: '20px'}}>
        <h5>Module Parameters</h5>
        Note: you may also need to visit the <a href="/#/settings">settings page</a> to set <ContextParametersDocumentationLink>context parameters</ContextParametersDocumentationLink>
        <div>
          {renderedParameters}
        </div>
      </div>
    );
  }

  renderModuleUrl = (app, module) => {
    const baseUrl = this.getAppBaseUrl(app);
    const generatedModuleUrl = this.buildGeneratedModuleUrl(app, module);
    return (
      <div style={{marginTop: '20px'}}>
        <h5>URLs</h5>

        <Label label='Descriptor module URL' />
        <Button
          appearance="link"
          href={baseUrl + module.url}
          target="_blank"
          iconAfter={<ShortcutIcon size="small" label="Module URL" />}
        >
          {module.url}
        </Button>

        <Label label='Generated app URL' />
        <Button
          appearance="link"
          href={generatedModuleUrl}
          target="_blank"
          iconAfter={<ShortcutIcon size="small" label="Module URL" />}
        >
          {generatedModuleUrl}
        </Button>
      </div>
    );
  };

  renderAppRenderingControls = (app, module) => {
    return (
      <div>
        <div>
          <Label label='Show app iframe border' />
          <ToggleStateless
            value="show-app-frame-border"
            isChecked={this.state.showAppFrameBorder}
            onChange={e => this.setState({showAppFrameBorder: e.target.checked})}
          />
        </div>
        {module.parameters && module.parameters.length ? this.renderModuleParameters(module) : null}
        {this.renderModuleUrl(app, module)}
      </div>
    );
  };

  renderControls = (app, module) => {
    return (
      <div
        style={{
          marginTop: '-20px',
          marginBottom: '20px'
        }}
      >
        <div>
          <Label label='Show app rendering controls' />
          <ToggleStateless
            value="show-app-rendering-controls"
            isChecked={this.state.showAppRenderingControls}
            onChange={e => this.setState({showAppRenderingControls: e.target.checked})}
          />
        </div>
        {this.state.showAppRenderingControls ? this.renderAppRenderingControls(app, module) : null}
        <hr/>
      </div>

    );
  };

  getAppBaseUrl = (app) => {
    return app.baseUrl ? app.baseUrl : window.location.origin;
  };

  renderModuleIframe = (app, module) => {
    const handleHideInlineDialog = () => {
      // Nothing to do here - I'm not in an inline dialog.
    };
    const connectIframeProvider = new DefaultConnectIframeProvider(handleHideInlineDialog);
    const options = {
      'origin': app.baseUrl ? app.baseUrl : window.location.origin,
      'hostOrigin': window.location.origin,
      'autoresize': this.props.settings.autoresize,
      'widthinpx': this.props.settings.widthinpx,
      'isFullPage': this.props.settings.isFullPage,
      'targets': {
        'env': {
          'resize': 'both'
        }
      }
    };
    const url = this.buildGeneratedModuleUrl(app, module);
    const borderStyle = this.state.showAppFrameBorder ? '1px solid #DE350B' : '0px';
    return (
      <div style={{
        width: iframeMaxWidth,
        height: iframeMaxHeight,
        border: borderStyle
      }}
      >
        <ConnectIframe
          connectHost={connectHost}
          appKey={app.key}
          moduleKey={module.key}
          loadingIndicator={IframeLoadingIndicator}
          timeoutIndicator={IframeTimeoutIndicator}
          failedToLoadIndicator={IframeFailedIndicator}
          url={url}
          options={options}
          height={appMinHeight}
          width={appMinWidth}
          storeNestedIframeJSON={storeNestedIframeJSON}
          connectIframeProvider={connectIframeProvider}
        >
          {this.nestedConnectIframes(10, connectHost, url, options)}
        </ConnectIframe>
      </div>
    );
  }

  areAllPropertyValuesProvided = (url) => {
    const leftBracketIndex = url.indexOf('{');
    const rightBracketIndex = url.indexOf('}');
    return leftBracketIndex === -1 && rightBracketIndex === -1;
  };

  renderShowAppRenderingControlsButton = () => {
    return (
      <div
        style={{
          display: 'inline-block',
          marginLeft: '20px'
        }}
      >
        <Button
          appearance='primary'
          iconBefore={<AddCircleIcon primaryColor='#fff' secondaryColor='#0052CC' label='Set URL parameters' />}
          target='_blank'
          onClick={this.onShowUrlParameterSettings}
        >
          Set URL parameters
        </Button>
      </div>
    );
  };

  render() {
    const appModulePair = this.determineModule();
    const app = appModulePair.app;
    const module = appModulePair.module;
    const url = this.buildGeneratedModuleUrl(app, module);
    const showUrlParameterSettingsButton = this.state.showAppRenderingControls ? null : this.renderShowAppRenderingControlsButton();
    const renderedModule = this.areAllPropertyValuesProvided(url) ?
      this.renderModuleIframe(app, module) :
      (
        <div>
          The module URL has unresolved parameters.
          {showUrlParameterSettingsButton}
        </div>
      );
    return (
      <ContentWrapper>
        <PageTitle>{module.name.value}</PageTitle>
        {this.renderControls(app, module)}
        {renderedModule}
      </ContentWrapper>
    );
  }
}

function mapStateToProps(state, props) {
  return {
    settings: state.settings
  };
}

export default connect(mapStateToProps)(GenericAppHostPage);