import connectHostRequest from 'atlassian-connect-js-request';

/* global Xhr */

/**
 * This module is the equivalent of connect-plugin-request.js which is included by the Atlassian Connect plugin. The
 * source for code is at https://bitbucket.org/atlassian/atlassian-connect-js-extra/src/06fb6a8517aeda5fed4cecd729689b3d2be38521/packages/atlassian-connect-js-request/src/plugin/index.js.
 */
const requestModule = {

  request: function(urlOrOptions, callbackOrOptions) {

    const promise = new Promise(function (resolve, reject) {

      let options = undefined;
      let callback = undefined;

      if (typeof urlOrOptions === "string") {
        const url = urlOrOptions;
        if (typeof callbackOrOptions === "object") {
          callbackOrOptions.url = url;
          options = callbackOrOptions;
        } else {
          options = {
            url: url
          };
        }
      } else {
        options = urlOrOptions;
        callback = callbackOrOptions;
      }

      // find callbacks in old syntax
      if (options.success) {
        var success = options.success;
        delete options.success;
        var error = options.error || function () {};
        delete options.error;
        callback = function (err, response, body) {
          if (err === false) {
            success(body, "success", Xhr(response));
          } else {
            error(Xhr(response), "error", err);
          }
        };
      } else if (typeof callback !== "function") {
        callback = function (err, response, body) {
          if (err === false) {
            resolve({ body: body, xhr: Xhr(response) });
          } else {
            reject({ xhr: Xhr(response), err: err });
          }
        };
      }

      if (options.contentType === 'multipart/form-data') {
        const wrapBlob = function(value) {
          if (value instanceof Blob && value.name) {
            return { blob: value, name: value.name, _isBlob: true };
          }
          return value;
        };
        Object.keys(options.data).forEach(function (key) {
          var item = options.data[key];
          if (Array.isArray(item)) {
            item.forEach(function (val, index) {
              options.data[key][index] = wrapBlob(val);
            });
          } else {
            options.data[key] = wrapBlob(item);
          }
        });
      }
      connectHostRequest.request(options, callback);
    });

    window._AP.request = this;

    return promise;
  }

};

export default requestModule;
