
export default class AppDAO {

  static appDataKey = "refapp-apps";

  static isRefappEnabled() {
    const apps = AppDAO.getEnabledApps();
    return apps.some(app => app.key === 'refapp' && app.enabled);
  }

  static getEnabledModules() {
    const modules = [];
    AppDAO.iterateModules(AppDAO.enabledAppsFilter, (app, moduleType, module) => {
      modules.push(module);
    });
    return modules;
  }

  static allAppsFilter() {
    return true;
  }

  static enabledAppsFilter(app) {
    return app.enabled;
  }

  /**
   * This method iterates modules that pass a given filter.
   * @param appsFilter a filter to apply to each app. Two obvious choices are
   * AppDAO.allAppsFilter and AppDAO.enabledAppsFilter.
   * @param callback a function to be called for each module that passed the filter. The
   * arguments to the callback are (app, moduleType, module).
   * @returns {Array} an array of the accumulated results returned by callback which
   * is handy inside React render operations.
   */
  static iterateModules(appsFilter, callback) {
    const accumulatedResults = [];
    const apps = AppDAO.getEnabledApps();
    for (let appIndex = 0; appIndex < apps.length; appIndex++) {
      const app = apps[appIndex];
      if (appsFilter(app)) {
        const app = apps[appIndex];
        const results = AppDAO.iterateModulesInApp(app, callback);
        if (results && results.length) {
          accumulatedResults.push.apply(accumulatedResults, results);
        }
      }
    }
    return accumulatedResults;
  }

  static iterateModulesInApp(app, callback) {
    const accumulatedResults = [];
    for (let moduleType in app.modules) {
      if (app.modules.hasOwnProperty(moduleType)) {
        const modules = app.modules[moduleType];
        for (let moduleIndex = 0; moduleIndex < modules.length; moduleIndex++) {
          const module = modules[moduleIndex];
          const result = callback(app, moduleType, module);
          if (result) {
            accumulatedResults.push(result);
          }
        }
      }
    }
    return accumulatedResults;
  }

  static getEnabledApps() {
    const allApps = AppDAO.getAllApps();
    const enabledApps = allApps.filter(function(app) {
      return app.enabled
    });
    return enabledApps;
  }

  static getAllApps() {
    let apps = null;
    let appsJson = localStorage.getItem(AppDAO.appDataKey);
    if (appsJson) {
      apps = JSON.parse(appsJson);
    } else {
      apps = AppDAO.getDefaultAllApps();
    }
    return apps;
  }

  static createApp() {
    const suffix = new Date().getMilliseconds();
    const app = {
      enabled: true,
      key: 'hello-world-copy-' + suffix,
      name: 'Hello World Copy ' + suffix,
      description: 'A simple hello world example.',
      modules: {
        generalPages: [{
          key: 'page',
          name: {
            value: 'Hello World Copy ' + suffix
          },
          url: '/#/app/hello-world-app/'
        }]
      }
    }
    return app;
  }

  static deleteApp(appToDelete) {
    const originalApps = AppDAO.getAllApps();
    const apps = [];
    for (let appIndex = 0; appIndex < originalApps.length; appIndex++) {
      const app = originalApps[appIndex];
      if (app.key !== appToDelete.key) {
        apps.push(app)
      }
    }
    AppDAO.storeApps(apps);
    return apps;
  }

  static ensureKeyIsUnique(appToCheck) {
    const apps = AppDAO.getAllApps();
    let count = 0;
    for (let appIndex = 0; appIndex < apps.length; appIndex++) {
      const app = apps[appIndex];
      if (app.key === appToCheck.key) {
        count++;
      }
    }
    if (count > 0) {
      appToCheck.key = appToCheck.key + '-' + new Date().getMilliseconds();
      AppDAO.ensureKeyIsUnique(appToCheck);
    }
  }

  static updateApp(appToUpdate, originalAppKey) {
    const apps = AppDAO.getAllApps();
    for (let appIndex = 0; appIndex < apps.length; appIndex++) {
      const app = apps[appIndex];
      if (app.key === originalAppKey) {
        apps[appIndex] = appToUpdate;
      }
    }
    AppDAO.storeApps(apps);
    return apps;
  }

  static storeApps(apps) {
    const appsJson = JSON.stringify(apps);
    localStorage.setItem(AppDAO.appDataKey, appsJson);
  }

  static resetToDefault() {
    const apps = AppDAO.getDefaultAllApps();
    AppDAO.storeApps(apps);
    return apps;
  }

  static getDefaultAllApps() {
    const apps = [{
      enabled: true,
      key: 'perf',
      name: 'Performance',
      description: 'Measure timings of actions',
      modules: {
        generalPages: [{
          key: 'performance',
          name: {
            value: 'Performance Page'
          },
          url: '/#/app/perf-app-home/'
        }],
        webPanels: [{
          key: 'perf-dialog',
          name: {
            value: 'Dialog Module'
          },
          url: '/#/app/perf-app-dialog/'
        }, {
          key: 'perf-dialog-cacheable',
          name: {
            value: 'Cacheable Dialog Module'
          },
          url: '/#/app/perf-app-dialog/',
          cacheable: true
        }]
      }
    }, {
      enabled: true,
      key: 'hello-world',
      name: 'Hello World',
      description: 'A simple hello world example.',
      modules: {
        generalPages: [{
          key: 'page',
          name: {
            value: 'Hello World Page'
          },
          url: '/#/app/hello-world-app/'
        }], webPanels: [{
          key: 'panel',
          name: {
            value: 'Hello World Panel'
          },
          url: '/#/app/hello-world-app/'
        }]
      }
    }, {
      enabled: true,
      key: 'request-refapp',
      name: 'Request App',
      description: 'An app demonstrating request module capabilities.',
      modules: {
        generalPages: [{
          key: 'request-refapp-general-page',
          name: {
            value: 'Request Page'
          },
          url: '/#/app/request-app/'
        }], webPanels: [{
          key: 'request-refapp-panel',
          name: {
            value: 'Request Panel'
          },
          url: '/#/app/request-app/'
        }]
      }
    }, {
      enabled: true,
      key: 'user-refapp',
      name: 'User App',
      description: 'An app demonstrating the user module\'s capabilities.',
      modules: {
        generalPages: [{
          key: 'user-refapp-general-page',
          name: {
            value: 'User Page'
          },
          url: '/#/app/user-app/'
        }]
      }
    }, {
      enabled: true,
      key: 'refapp',
      name: 'Kitchen Sink App',
      description: 'A reference app to demonstrate most functionality.',
      modules: {
        generalPages: [{
          key: 'kitchen-sink',
          name: {
            value: 'Kitchen Sink Page'
          },
          url: '/#/app/refapp-app-home/'
        }],
        webPanels: [{
          key: 'basic-dialog-module',
          name: {
            value: 'Kitchen Sink Panel'
          },
          url: '/#/app/refapp-app-home/'
        },{
          key: 'basic-dialog-module-1',
          name: {
            value: 'Kitchen Sink Panel 2'
          },
          url: '/#/app/refapp-app-home/'
        },{
          key: 'basic-dialog-module-2',
          name: {
            value: 'Kitchen Sink Panel 3'
          },
          url: '/#/app/refapp-app-home/'
        }]
      }
    }, {
      enabled: true,
      "key": "atlassian-cards",
      "name": "Atlassian Cards",
      "description": "Dynamic Content Macro for rendering links as Atlassian Cards",
      "baseUrl": "https://d2ax30yw7i7dht.cloudfront.net",
      "vendor": {
        "name": "Atlassian Labs",
        "url": "https://www.atlassian.com"
      },
      "authentication": {
        "type": "none"
      },
      "modules": {
        "dynamicContentMacros": [
          {
            "key": "atlassian-card",
            "name": {
              "value": "Atlassian Card"
            },
            "outputType": "inline",
            "description": {
              "value": "Display content from a URL using the Atlassian Card service. Supports wide, narrow and small formats."
            },
            "parameters": [
              {
                "identifier": "content",
                "name": {
                  "value": "Content URL"
                },
                "description": {
                  "value": "The URL to the site you want Card rendering for."
                },
                "type": "string",
                "required": true,
                "multiple": false
              },
              {
                "identifier": "format",
                "name": {
                  "value": "Format"
                },
                "description": {
                  "value": "Format can be wide, narrow or small."
                },
                "type": "enum",
                "required": true,
                "multiple": false,
                "defaultValue": "wide",
                "values": [
                  "small",
                  "narrow",
                  "wide"
                ]
              },
              {
                "identifier": "nothumb",
                "name": {
                  "value": "No thumb"
                },
                "description": {
                  "value": "Set to true to exclude thumbnail image"
                },
                "type": "enum",
                "required": true,
                "multiple": false,
                "values": [
                  "false",
                  "true"
                ],
                "defaultValue": "false"
              }
            ],
            "categories": [
              "external-content"
            ],
            "method": "get",
            "url": "/card-renderer.html?url={content}&format{format}&nothumb={nothumb}",
            "icon": {
              "url": "/connect/img/88.png"
            }
          }
        ]
      },
      "version": "1.0.5-AC"
    }];
    return apps;
  }

}
