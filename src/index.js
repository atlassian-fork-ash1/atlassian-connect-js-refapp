import React from "react";
import ReactDOM from "react-dom";
import MainApp from "./product";
import HelloWorldApp from "./apps/helloworldapp/HelloWorldApp";
import RequestApp from "./apps/requestapp/RequestApp";
import RefApp from "./apps/refapp";
import RefappUtil from "./common/RefappUtil";
import connectHost from 'atlassian-connect-js';
import request from './product/requestModule';
import UserApp from "./apps/userapp/UserApp";
import user from './product/mockUserModule';
import PerformanceApp from './apps/performanceapp';

// TODO: ACJS-861: Remove AJS dependency from request
window.AJS = {
  contextPath: function() {
    return '';
  }
};

connectHost.defineModule(request);
connectHost.defineModule('user', user);

function loadResource(attributes, callback) {
  const fileref = document.createElement('script');
  Object.keys(attributes).forEach(function(key) {
    const value = attributes[key];
    if (value) {
      fileref.setAttribute(key, value);
    }
  });
  fileref.onreadystatechange = fileref.onload = () => {
    if (!callback.done && (!fileref.readyState || /loaded|complete/.test(fileref.readyState))) {
      callback.done = true;
      callback();
    }
  };
  document.getElementsByTagName("head")[0].appendChild(fileref);
}

const hash = window.location.hash;
if (hash.indexOf('#/app/') === 0) {
  let appComponent = null;
  if (hash.indexOf('#/app/refapp') === 0) {
    appComponent = <RefApp/>;
  } else if (hash.indexOf('#/app/hello-world-app') === 0) {
    appComponent = <HelloWorldApp/>;
  } else if (hash.indexOf('#/app/request-app') === 0) {
    appComponent = <RequestApp/>;
  } else if (hash.indexOf('#/app/user-app') === 0) {
    appComponent = <UserApp/>;
  } else if (hash.indexOf('#/app/perf-app') === 0) {
    appComponent = <PerformanceApp/>;
  }
  const cassParam = RefappUtil.getUrlParameterByName('caas');
  const src = '/atlassian-connect/' + (cassParam === 'true' ? 'iframe.js' : 'all.js');
  const dataOptions = RefappUtil.getUrlParameterByName('data-options');
  loadResource(
    {
      src,
      type: 'text/javascript',
      'data-options': dataOptions
    },
    () => ReactDOM.render(appComponent, document.getElementById('app-root'))
  );
} else {

  // ACJS-883: The following block is only here for compatibility back to when we used push
  // state URLs rather than hash based URLs.
  if (window.location.href.indexOf('/app/') >= 0) {
    let appComponent = null;
    if (window.location.href.indexOf('/app/refapp') >= 0) {
      appComponent = <RefApp/>;
    } else if (window.location.href.indexOf('/app/hello-world-app') >= 0) {
      appComponent = <HelloWorldApp/>;
    } else if (window.location.href.indexOf('/app/request-app') >= 0) {
      appComponent = <RequestApp/>;
    } else if (window.location.href.indexOf('/app/perf-app') >= 0) {
      appComponent = <PerformanceApp/>;
    }
    const cassParam = RefappUtil.getUrlParameterByName('caas');
    const src = '/atlassian-connect/' + (cassParam === 'true' ? 'iframe.js' : 'all.js');
    const dataOptions = RefappUtil.getUrlParameterByName('data-options');
    loadResource(
      {
        src,
        type: 'text/javascript',
        'data-options': dataOptions
      },
      () => ReactDOM.render(appComponent, document.getElementById('app-root'))
    );
  } else {
    /* ACJS Reference Product */
    ReactDOM.render(<MainApp />, document.getElementById('app-root'));
  }
}