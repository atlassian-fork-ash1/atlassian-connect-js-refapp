import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import "@atlaskit/css-reset";
import {Link} from "react-router-dom";
import Page from "@atlaskit/page";
import Navigation, {AkNavigationItem, AkContainerTitle, presetThemes} from "@atlaskit/navigation";
import DashboardIcon from "@atlaskit/icon/glyph/dashboard";
import AppIcon from "@atlaskit/icon/glyph/addon";

import RefappUtil from '../../../common/RefappUtil';
import SharedAppUtil from '../../../apps/shared/SharedAppUtil';
import DialogContent from '../pages/DialogContent.jsx';

const noFilterProductName = 'no-filter';
const navLinks = [
  ['/app/perfapp-app-home', 'Performance Home', DashboardIcon, noFilterProductName],
  ['/app/perfapp-app-cacheable-dialog', 'Dialog', AppIcon, noFilterProductName]
];

export default class PerformanceApp extends PureComponent {
  state = {
    isCreateDrawerOpen: false,
    isModalOpen: false,
    isSearchDrawerOpen: false
  };

  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object,
  };

  static propTypes = {
    navOpenState: PropTypes.object,
    onNavResize: PropTypes.func,
    theme: PropTypes.string
  };

  static childContextTypes = {
    showInfoMessage: PropTypes.func,
    showModal: PropTypes.func,
    addFlag: PropTypes.func,
    width: PropTypes.string,
    height: PropTypes.string,
    minWidth: PropTypes.string,
    minHeight: PropTypes.string,
    onWidthChange: PropTypes.func,
    onHeightChange: PropTypes.func,
  };

  componentWillMount() {
    const soloPage = RefappUtil.getUrlParameterByName('soloPage');
    this.setState({soloPage: soloPage});
    SharedAppUtil.iterateAppOptions((key, value) => {
      if (key === 'host-product-name') {
        this.setState({hostProductName: value});
      }
    });
  }

  getChildContext() {
    return {
      showInfoMessage: this.showInfoMessage,
      showModal: this.showModal,
      addFlag: this.addFlag,
      width: this.state.width,
      height: this.state.height,
      onWidthChange: this.onWidthChange,
      onHeightChange: this.onHeightChange
    };
  }

  onWidthChange = (event) => {
    this.setState({
      width : event.target.value
    });
  }

  onHeightChange = (event) => {
    this.setState({
      height : event.target.value
    });
  }

  render() {
    const filteredLinks = navLinks.filter((navLink) => {
      const [, , , filteredProductName] = navLink;
      if (filteredProductName === noFilterProductName) {
        return true;
      } else {
        return filteredProductName.indexOf(this.state.hostProductName) >= 0;
      }
    });
    let content = null;
    if (this.state.soloPage && this.state.soloPage.indexOf("perf-dialog") !== -1) {
      content = (
        <DialogContent moduleKey={this.state.soloPage} />
      );
    } else {
      let width;
      let height;
      switch (this.props.location.pathname) {
        case '/app/perfapp-app-nested-app' :
          width = null;
          height = null;
          break;
        case '/app/refapp-app-sizing' :
          width = this.state.width + 'px';
          height = this.state.height + 'px';
          break;
        default :
          width = '100%';
          height = '100%';
          break;
      }
      content = (
        <div style={{
          height,
          width
        }}>
          <Page navigationWidth={this.context.navOpenState.width} navigation={
            <Navigation
              containerTheme={presetThemes[this.props.theme]}
              globalTheme={presetThemes[this.props.theme]}
              headerComponent={() => (
                <AkContainerTitle
                  text="Performance"
                />
              )}
            >
              {
                filteredLinks.map(link => {
                  const [url, title, Icon] = link;
                  const pathname = this.context.router.route.location.pathname;
                  const queryIndex = url.indexOf('?');
                  const urlPath = queryIndex >= 0 ? url.substring(0, queryIndex) : url;
                  const isSelected = urlPath === pathname;
                  return (
                    <Link key={url} to={url}>
                      <AkNavigationItem
                        icon={<Icon label={title}/>}
                        text={title}
                        isSelected={isSelected}
                      />
                    </Link>
                  );
                })
              }
            </Navigation>
          }
          >
            {this.props.children}
          </Page>
        </div>
      );
    }
    return content;
  }
}
