import { akGridSize } from '@atlaskit/util-shared-styles';
const gridSizeInt = parseInt(akGridSize, 10);
const refAppMinHeight = '676';
const refAppMinWidth = '800';
const refAppInitialWidth = '900';

export { gridSizeInt, refAppMinWidth, refAppInitialWidth, refAppMinHeight };
