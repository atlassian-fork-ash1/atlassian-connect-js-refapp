import React, { PureComponent } from 'react';
import Button from '@atlaskit/button';
import ContentWrapper from '../components/ContentWrapper';
import FieldRadioGroup from '@atlaskit/field-radio-group';
import PageTitle from '../components/PageTitle';
import Tabs from '@atlaskit/tabs';
import CodeGenerator from './CodeGenerator';

/* global AP */

class FlagDetails extends PureComponent {
  closeFlag = () => {
    console.log('In closeFlag');
    this.props.closeFlag(this.props.flag);
  };

  render() {
    const flagContainerStyle = {
      margin: '0.5em 0 0.5em 0',
      padding: '0.5em',
      border: '1px solid #ccc',
      borderRadius: '5px'
    };
    const flagIdStyle = {
      marginRight: '20px'
    };
    return (
      <div style={flagContainerStyle} onClick={event => this.closeFlag()}>
        <span style={flagIdStyle}>{this.props.flag.flagKey}</span>
        <Button appearance="default" onClick={event => this.closeFlag()}>
          Close
        </Button>
      </div>
    );
  }
}

class NoFlagsIndicator extends PureComponent {
  render() {
    const messageStyle = {
      margin: '0.5em 0 0.5em 0'
    };
    return (
      <div style={messageStyle}>Oh my! There shouldn't be any flags open.</div>
    );
  }
}

export default class FlagPage extends PureComponent {
  static HI_ACTION_ID = 'sayHiAction';
  static BOO_ACTION_ID = 'sayBooAction';

  static lastFlagKey = 0;

  state = {
    selectedFlagType: 'info',
    selectedCloseOption: 'auto',
    flags: [] // current flags in chronological order by creation time
  };

  constructor(props) {
    super(props);
    AP.events.on('flag.action', data => {
      console.log(
        'Received an action event on flag id',
        data.flagIdentifier,
        ', flag action is ',
        data.actionIdentifier,
        '.'
      );
      if (data.actionIdentifier === FlagPage.HI_ACTION_ID) {
        alert('Hi');
      } else if (data.actionIdentifier === FlagPage.BOO_ACTION_ID) {
        alert('Boo');
      }
    });
    AP.events.on('flag.close', data => {
      console.log(
        'Received event that flag id',
        data.flagIdentifier,
        'has been closed.'
      );
      this.closeFlagWithId(data.flagIdentifier);
    });
  }

  createFlag = () => {
    FlagPage.lastFlagKey++;
    const flagKey = 'Flag ' + FlagPage.lastFlagKey;
    //const flagType = this.state.selectedFlagType;
    //const flagTypeText = `${/^[aeiou]/.test(flagType.toLowerCase()) ? 'an' : 'a'} '${flagType}'`;
    const flag = AP.flag.create(this.getFlagOptions());
    flag.flagKey = flagKey;
    const newFlags = this.state.flags.concat([flag]);
    this.setState({
      flags: newFlags
    });
    console.log('Created flag:', flag);
  };

  getFlagOptions = () => {
    const flagKey = 'Flag ' + FlagPage.lastFlagKey;
    const flagType = this.state.selectedFlagType;
    const flagTypeText = `${
      /^[aeiou]/.test(flagType.toLowerCase()) ? 'an' : 'a'
    } '${flagType}'`;
    const flag = {
      title: `${flagKey}: This is ${flagTypeText} flag.`,
      body: `My close option is '${this.state.selectedCloseOption}'.`,
      type: flagType,
      close: this.state.selectedCloseOption,
      actions: {
        sayHiAction: 'Say hi action',
        sayBooAction: 'Say boo action'
      }
    };

    return flag;
  };

  closeFlag = flag => {
    this.closeFlagWithId(flag._id);
  };

  closeFlagWithId = flagId => {
    console.log('Closing flag with ID ', flagId, '...');
    this.state.flags.forEach(flag => {
      if (flag._id === flagId) {
        flag.close();
      }
    });
    const filteredFlags = this.state.flags.filter(flag => flag._id !== flagId);
    this.setState({
      flags: filteredFlags
    });
  };

  render() {
    const flagTypeItems = [
      { name: 'flagType', value: 'error', label: 'error' },
      { name: 'flagType', value: 'info', label: 'info', defaultSelected: true },
      { name: 'flagType', value: 'normal', label: 'normal' },
      { name: 'flagType', value: 'success', label: 'success' },
      { name: 'flagType', value: 'warning', label: 'warning' }
    ];
    const closeOptionItems = [
      { name: 'closeOption', value: 'manual', label: 'manual' },
      {
        name: 'closeOption',
        value: 'auto',
        label: 'auto',
        defaultSelected: true
      },
      { name: 'closeOption', value: 'never', label: 'never' }
    ];
    const allFlagDetails = this.state.flags.length ? (
      this.state.flags.map(flag => {
        return (
          <FlagDetails key={flag._id} flag={flag} closeFlag={this.closeFlag} />
        );
      })
    ) : (
      <NoFlagsIndicator />
    );
    return (
      <ContentWrapper>
        <PageTitle>Flag</PageTitle>
        <Tabs
          tabs={[
            {
              content: (
                <div>
                  <h3>Create a flag</h3>

                  <FieldRadioGroup
                    items={flagTypeItems}
                    label="Select a flag type:"
                    onRadioChange={e => {
                      this.setState({
                        selectedFlagType: e.target.value
                      });
                    }}
                  />

                  <FieldRadioGroup
                    items={closeOptionItems}
                    label="Select how the flag will close:"
                    onRadioChange={e => {
                      this.setState({
                        selectedCloseOption: e.target.value
                      });
                    }}
                  />

                  <Button
                    appearance="primary"
                    onClick={event => this.createFlag()}
                  >
                    Create flag
                  </Button>

                  <h3>Current flags</h3>
                  {allFlagDetails}
                </div>
              ),
              defaultSelected: true,
              label: 'Create a flag'
            },
            {
              content: (
                <CodeGenerator
                  api={CodeGenerator.API.flag}
                  options={this.getFlagOptions()}
                />
              ),
              label: 'Get Code'
            }
          ]}
        />
      </ContentWrapper>
    );
  }
}
