import React, { PureComponent } from "react";
import Button from "@atlaskit/button";
import ContentWrapper from "../components/ContentWrapper";
import PageTitle from "../components/PageTitle";
import FieldText from '@atlaskit/field-text';

/* global AP */

export default class ScrollPage extends PureComponent {

  state = {
    verticalScrollPosition: 0
  };

  componentWillMount(props) {
    AP.events.on('scroll.nearTop', this.logEvent);
    AP.events.on('scroll.nearBottom', this.logEvent);
  }

  componentWillUnmount(){
    AP.events.off('scroll.nearTop', this.logEvent);
    AP.events.off('scroll.nearBottom', this.logEvent);
  }

  logEvent = (data) => {
    AP.flag.create({
      title: 'Scroll event',
      body: `scrolling boundary triggered`,
      type: 'success',
      close: 'auto'
    });
  }

  getScrollPosition = () => {
    AP.scrollPosition.getPosition(function(obj) {
      AP.flag.create({
        title: 'Scroll position',
        body: `scrollY:${obj.scrollY} scrollX:${obj.scrollY} width:${obj.width} height:${obj.height}`,
        type: 'success',
        close: 'auto'
      });

    });
  }

  setScrollPosition = (value) => {
    AP.scrollPosition.setVerticalPosition(parseInt(this.state.verticalScrollPosition, 10));
  }

  render() {
    return (
      <ContentWrapper>
        <PageTitle>Scroll</PageTitle>
        <h3>Get Scroll Position</h3>
        <Button
            appearance="primary"
            onClick={(event) => this.getScrollPosition()}
        >Get Scroll Position</Button>
        <hr />
        <h3>Set Vertical Scroll Position</h3>
        <FieldText
          label='position'
          onChange={e => this.setState({verticalScrollPosition: e.target.value})}
          value="0"
        />
        <Button
          appearance="primary"
          onClick={(event) => {this.setScrollPosition()}}
        >Set Scroll Position</Button>
      </ContentWrapper>
    );
  }
}
