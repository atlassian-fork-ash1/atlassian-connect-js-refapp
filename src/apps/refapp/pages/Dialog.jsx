import Tabs from '@atlaskit/tabs';
import Button from "@atlaskit/button";
import React, { PureComponent } from "react";
import FieldText from '@atlaskit/field-text';
import Page, { Grid, GridColumn } from '@atlaskit/page';
import DialogPage from './DialogPage';

/* global AP */

export default class Dialog extends PureComponent {

  state = {
    data: {key: 'value'},
    identifier: 'submit',
    createId: '',
    createText: 'User Button',
    height: '100%'
  };

  componentWillMount() {
    AP.dialog.getCustomData(customData => this.setState({customData}));
    ['dialog.submit', 'dialog.cancel', 'dialog.button.click'].forEach( eventName => {
      AP.events.on(eventName, data => {
        if (data && data.button) {
          const isSubmitOrCancel = data.button.identifier === 'submit' || data.button.identifier === 'cancel';
          AP.flag.create({
            title: `Dialog button event`,
            body: `'${eventName}' event received within the dialog. Event details: ${JSON.stringify(data, null, 2)} `,
            type: 'success',
            close: isSubmitOrCancel ? 'auto' : 'manual'
          });
        }
      });
    });
  }

  render() {
    return (
      <Page>
        <Tabs tabs={[
          {
            content: (
              <Grid>
                <GridColumn>
                  <br />
                  <Button onClick={() => AP.dialog.getCustomData(customData => alert(JSON.stringify(customData)))} >AP.dialog.getCustomData()</Button>
                  <br />
                  <Button onClick={() => AP.dialog.close(this.state.data)}>AP.dialog.close(data)</Button>
                  <br />
                  <Button onClick={() => AP.dialog.disableCloseOnSubmit()}>AP.dialog.disableCloseOnSubmit()</Button>
                </GridColumn>
                <GridColumn>
                  <FieldText
                    label='data'
                    onChange={e => this.setState({data: JSON.parse(e.target.value)})}
                    value={JSON.stringify(this.state.data)}
                  />
                </GridColumn>
              </Grid>
            ),
            defaultSelected: true,
            label: 'General'
          },
          {
            content: (
              <div>
                <FieldText
                  label='id'
                  onChange={e => this.setState({identifier: e.target.value})}
                  value={this.state.identifier}
                />
                <Button onClick={() => AP.dialog.getButton(this.state.identifier).disable()}>AP.dialog.getButton('{this.state.identifier}').disable()</Button><br />
                <Button onClick={() => AP.dialog.getButton(this.state.identifier).enable()}>AP.dialog.getButton('{this.state.identifier}').enable()</Button><br />
                <Button onClick={() => AP.dialog.getButton(this.state.identifier).toggle()}>AP.dialog.getButton('{this.state.identifier}').toggle()</Button><br />
                <Button onClick={() => AP.dialog.getButton(this.state.identifier).hide()}>AP.dialog.getButton('{this.state.identifier}').hide()</Button><br />
                <Button onClick={() => AP.dialog.getButton(this.state.identifier).show()}>AP.dialog.getButton('{this.state.identifier}').show()</Button><br />
                <Button onClick={() => AP.dialog.getButton(this.state.identifier).isEnabled(enabled => alert(enabled))}>AP.dialog.getButton('{this.state.identifier}').isEnabled()</Button><br />
                <Button onClick={() => AP.dialog.getButton(this.state.identifier).isHidden(hidden => alert(hidden))}>AP.dialog.getButton('{this.state.identifier}').isHidden()</Button><br />
              </div>
            ),
            label: 'Button API'
          },
          {
            content: (
              <div>
                <FieldText
                  label='Button ID'
                  onChange={e => this.setState({createId: e.target.value})}
                  value={this.state.createId}
                />
                <FieldText
                  label='Button text'
                  onChange={e => this.setState({createText: e.target.value})}
                  value={this.state.createText}
                />
                <div style={{marginTop: '20px'}}>
                  <Button
                    appearance="primary"
                    onClick={
                      () => AP.dialog.createButton({identifier: this.state.createId, text: this.state.createText})
                    }>AP.dialog.createButton()
                  </Button>
                </div>
              </div>
            ),
            label: 'createButton()'
          },
          {
            content: (
              <div style={{
                height: this.state.height+'px'
              }}>
                <FieldText
                  type='number'
                  label='Div height (px)'
                  value={this.state.height}
                  onChange={e => this.setState({height: e.target.value})}
                />
                &nbsp;
                <Button onClick={() => AP.resize()}>AP.resize()</Button>
              </div>
            ),
            label: 'Dialog Size'
          },
          {
            content: (
              <DialogPage nested={true}/>
            ),
            label: 'Create another'
          }
        ]} />
      </Page>
    );
  }
}