import React, { PureComponent } from 'react';
import PageTitle from '../components/PageTitle';
import ContentWrapper from '../components/ContentWrapper';
import IframeInsertion from '../../../common/components/IframeInsertion';

export default class NestedAppPage extends PureComponent {
  render() {
    return (
      <ContentWrapper>
        <PageTitle>Nested App Page</PageTitle>
        <IframeInsertion />
      </ContentWrapper>
    );
  }
}
