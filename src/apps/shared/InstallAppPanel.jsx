import React, { Component } from "react";
import Button from '@atlaskit/button';

export default class InstallAppPanel extends Component {

  state = {
    collapsedMode: true
  };

  onCloseExpandedModeClick = () => {
    this.setState({
      collapsedMode: true
    });
  };

  onInstallButtonClick = () => {
    this.setState({
      collapsedMode: false
    });
  };

  renderExpandedModeForSingleDescriptorUrl(appDescriptorUrl) {
    return (
      <div>
        <p>
          The descriptor URL of this app is <a href={appDescriptorUrl} target="_blank" rel="noopener noreferrer">{appDescriptorUrl}</a>.
          <Button
            appearance="subtle-link"
            onClick={this.onCloseExpandedModeClick}
          >
            Got it
          </Button>
        </p>
      </div>
    );
  }

  renderExpandedModeForMultipleDescriptorUrls(appDescriptorUrls) {
    return (
      <div>
        <p>
          This app can be installed using one of the follow descriptor URLs:
          The descriptor URL of this app is .
        </p>
        <ul>
          {
            appDescriptorUrls.map((appDescriptorUrl) => {
              return (
                <li>
                  <a href={appDescriptorUrl} target="_blank" rel="noopener noreferrer">{appDescriptorUrl}</a>
                </li>
              );
            })
          }
        </ul>
        <Button
          appearance="subtle-link"
          onClick={this.onCloseExpandedModeClick}
        >
          Got it
        </Button>
      </div>
    );
  }

  renderExpandedMode() {
    if (this.props.appDescriptorUrls && this.props.appDescriptorUrls.length === 1) {
      return this.renderExpandedModeForSingleDescriptorUrl(this.props.appDescriptorUrls[0]);
    } else {
      return this.renderExpandedModeForMultipleDescriptorUrls(this.props.appDescriptorUrls);
    }
  }

  renderCollapsedMode() {
    return (
      <div>
        <Button
          onClick={this.onInstallButtonClick}
        >
          Install {this.props.appName}...
        </Button>
      </div>
    );
  }

  render() {
    if (this.state.collapsedMode) {
      return this.renderCollapsedMode();
    } else {
      return this.renderExpandedMode();
    }
  };

}
