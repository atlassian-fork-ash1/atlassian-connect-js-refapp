import React, { PureComponent } from 'react';
import Button from '@atlaskit/button';
import SingleSelect from '@atlaskit/single-select'
import FieldTextArea from '@atlaskit/field-text-area';
import InstallAppPanel from '../shared/InstallAppPanel';
import { Label } from '@atlaskit/field-base'; //ref: AK-1621
import SharedAppUtil from '../shared/SharedAppUtil';
import Tag from '@atlaskit/tag';
import Toggle from '@atlaskit/toggle';
import VidShareScreenIcon from '@atlaskit/icon/glyph/vid-share-screen';
import Tabs from '@atlaskit/tabs';
import CodeGenerator from '../refapp/pages/CodeGenerator';

/* global AP */

export default class RequestApp extends PureComponent {

  constructor(props) {
    super(props);
    const exampleUrl = '/app-descriptors/kitchen-sink-confluence-app-connect-descriptor.json';
    this.state = {
      url: exampleUrl,
      method: 'GET',
      data: undefined,
      isDataValid: true,
      headers: undefined,
      isHeadersValid: true,
      cache: false,
      lastResponse: undefined
    };
  }

  onUrlFilterChange = (filter) => {
    this.setState({
      url: filter
    });
  };

  onUrlSelected = (selection) => {
    this.setState({
      url: selection.item.value
    });
  };

  onMethodSelected = (selection) => {
    this.setState({
      method: selection.item.value
    });
  };

  onDataEdit = (event) => {
    const text = event.target.value;
    try {
      const updatedData = JSON.parse(text);
      this.setState({
        data: updatedData,
        isDataValid: true
      });
    } catch (exception) {
      console.error('Unable to process or parse data: ' + exception);
      this.setState({
        headers: text,
        isDataValid: false
      });
    }
  };

  onHeadersEdit = (event) => {
    const text = event.target.value;
    try {
      const updatedHeaders = JSON.parse(text);
      this.setState({
        headers: updatedHeaders,
        isHeadersValid: true
      });
    } catch (exception) {
      console.error('Unable to process or parse headers: ' + exception);
      this.setState({
        headers: text,
        isHeadersValid: false
      });
    }
  };

  onSendRequestAndHandleWithCallback = () => {
    const requestOptions = this.getRequestOptions();
    const responseHandler = (errorOrData, response, xhrOrErrorThrown) => {
      this.setState({
        lastResponse: response
      });
      if (errorOrData === false) {
        AP.flag.create({
          title: 'Request sent successfully',
          body: 'status = ' + response.status + ', ' + response.statusText + ' (length = ' + response.responseText.length + ')',
          type: 'success',
          close: 'auto'
        });
      } else {
        AP.flag.create({
          title: 'Request failed to send',
          body: 'status = ' + response.status + ', ' + response.statusText + ' (' + xhrOrErrorThrown + ')',
          type: 'error',
          close: 'auto'
        });
      }
    };

    AP.request(requestOptions, responseHandler);
  };

  getRequestOptions = () => {
    const requestOptions = {
      url: this.state.url,
      type: this.state.method,
      data: this.state.data,
      headers: this.state.headers,
      cache: this.state.cache
    };

    return requestOptions;
  };

  parseResponseText = (text) => {
    // For the moment, just try and format it as JSON
    try {
      return {
        format: 'json',
        text: JSON.stringify(JSON.parse(text), null, 2)
      };
    } catch (exception) {
      if (text && text.toLowerCase().indexOf('html') >= 0) {
        return {
          format: 'html',
          text: text
        }
      } else {
        return {
          format: undefined,
          text: text
        }
      }
    }
  };

  renderResponse = (response) => {
    const parsedResponse = this.parseResponseText(response.responseText);
    return (
      <div>
        <h4>
          Status
        </h4>
        <p>
          {response.status + ', ' + response.statusText}
        </p>

        <h4>
          Body
        </h4>

        <pre>
          {parsedResponse.text}
        </pre>
      </div>
    );
  };

  renderObjectInout = (label, object, isInvalid, onChange, placeholder) => {
    var json = object ? JSON.stringify(object, null, 2) : '';
    return (
      <div
        style={{
          width: '100%',
        }}
      >
        <FieldTextArea
          label={label}
          value={json}
          autoFocus
          minimumRows={5}
          placeholder={placeholder}
          enableResize={true}
          shouldFitContainer={true}
          isSpellCheckEnabled={false}
          isInvalid={isInvalid}
          onChange={onChange}
        />
      </div>
    );
  };

  buildUrlItems = () => {
    const itemOf = (url) => {
      return {content: url, value: url};
    };
    return [{
      heading: 'ACJS Refapp Host URLs',
      items: [
        itemOf('/request-app-connect-descriptor.json'),
        itemOf('/ACJS.png'),
        itemOf('http://www.mocky.io/v2/'),
      ]
    }, {
      heading: 'Confluence URLs',
      items: [
        itemOf('/rest/api/user'),
        itemOf('/rest/api/content/search?cql=type=page and created > now("-4w")'),
        itemOf('/rest/api/content/search?cql=type=page and creator = currentUser()'),
        itemOf('/rest/api/content/search?cql=type=page and label = "foo"')
      ]
    }, {
      heading: 'Jira Platform URLs',
      items: [
        itemOf('/rest/api/2/user'),
        itemOf('/rest/api/2/jql/autocompletedata')
      ]
    }];
  };

  buildMethodItems = () => {
    const itemOf = (method) => {
      return {content: method, value: method};
    };
    return [{
      items: [
        itemOf('GET'),
        itemOf('POST'),
        itemOf('PUT'),
        itemOf('OPTIONS'),
        itemOf('DELETE'),
      ]
    }];
  };

  renderApAvailable = () => {
    const methodItems = this.buildMethodItems();
    return (
      <div style={{
        margin: '20px'
      }}
      >
        {this.renderInstallAppPanel()}

        <Tabs tabs={[
          {
            content: (
              <div>
                <h3>Request options</h3>

                <div>
                  <Tag text="Tip" color="red" />:
                  When this app is run in the context of the <a href="https://acjsrefapp.atl-paas.net/" target="_blank" rel="noopener noreferrer">ACJS Refapp</a>,
                  relative URLs will mostly return HTML due to the static hosting of the ACJS Refapp. To overcome this limitation, you
                  can use a service like <a href="https://www.mocky.io/" target="_blank" rel="noopener noreferrer">www.mocky.io</a> to mock responses.
                  Note, however, sending to another domain won't work in a real product that hosts the Atlassian Connect framework.
                </div>

                <Label label='URL' />
                <SingleSelect
                          onSelected={this.onUrlSelected}
                          onFilterChange={this.onUrlFilterChange}
                          hasAutocomplete
                          shouldFitContainer={true}
                          items={this.buildUrlItems()}
                      />
                {this.renderObjectInout('Data (JSON format)', this.state.data, !this.state.isDataValid, this.onDataEdit, '{"foo": "bar"}')}
                {this.renderObjectInout('Headers (JSON format)', this.state.headers, !this.state.isHeadersValid, this.onHeadersEdit, '{"Accept": "text/html"}')}

                <Label label='Cache' />
                <Toggle
                          label='Cache'
                          isChecked={this.state.cache}
                          isDefaultChecked={this.state.cache}
                          onChange={e => this.setState({cache: e.target.checked})}
                      />

                <Label label='Method' />
                <SingleSelect
                          defaultSelected={methodItems[0].items[0]}
                          onSelected={this.onMethodSelected}
                          shouldFitContainer={false}
                          items={methodItems}
                      />

                <br/><br/>

                <Button
                          appearance='primary'
                          iconAfter={<VidShareScreenIcon label='' />}
                          target='_blank'
                          onClick={this.onSendRequestAndHandleWithCallback}
                      >
                          Send request
                      </Button>

                <div>
                  <br/>
                  <h3>Last response</h3>
                  <br/>
                  {this.state.lastResponse ? this.renderResponse(this.state.lastResponse) : <p>(none)</p>}
                </div>
              </div>
                ),
            label: "Request Options",
            defaultSelected: true
          },

          {
            content: (
              <CodeGenerator api={CodeGenerator.API.request} options={this.getRequestOptions()} />
            ),
            label: "Get Code"
          }
        ]} />
      </div>
    );
  };

  renderApUnavailable = () => {
    return (
      <div>
        <p>
        It looks like the request module is unavailable. :(
        </p>
        {this.renderInstallAppPanel()}
      </div>
    );
  };

  renderInstallAppPanel = () => {
    const appDescriptorUrl = SharedAppUtil.buildAppDescriptorUrl('request-app-connect-descriptor.json');
    return (
      <InstallAppPanel
        appName="Request App"
        appDescriptorUrls={[appDescriptorUrl]}
      />
    );
  };

  render() {
    if (AP && AP.request) {
      return this.renderApAvailable();
    } else {
      return this.renderApUnavailable();
    }
  }

}
