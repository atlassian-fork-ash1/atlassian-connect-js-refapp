import AppDAO from '../product/appmanagement/AppDAO';
import { unexpiredJWT } from '../product/constants';
import RefappUrlBuilder from './RefappUrlBuilder';

function isCacheable(addonKey, moduleKey){
  return AppDAO.iterateModules(
    app => addonKey === app.key,
    (app, moduleType, module) => (moduleKey === module.key && module.cacheable)
  ).length > 0;
}


export default class RefappUtil {

  static getUrlParameterByName(name, url) {
    if (!url) {
      url = window.location.href;
    }
    name = name.replace(/[[\]]/g, "\\$&");
    const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)");
    const results = regex.exec(url);
    if (!results) {
      return null;
    }
    if (!results[2]) {
      return '';
    };
    return decodeURIComponent(results[2].replace(/\+/g, " "));
  }

  static resolveModule = (params, settings) => {
    let nextDialogParam = '';
    if (params.key.startsWith('basic-dialog-module')) {
      nextDialogParam = '&soloPage=Dialog&nextDialog=';
      if (params.key === 'basic-dialog-module') {
        nextDialogParam += '1';
      } else if (params.key === 'basic-dialog-module-1') {
        nextDialogParam += '2';
      }
    }

    const dataOptions =
      'resize:' + settings.resize +
      ';sizeToParent:' + settings.sizeToParent +
      ';margin:' + settings.margin +
      ';base:' + settings.base;

    const results = AppDAO.iterateModules(
      app => params.addon_key === app.key,
      (app, moduleType, module) => params.key === module.key ? module.url : false
    );

    if (params.key.indexOf('perf-dialog') !== -1) {
      nextDialogParam = '&soloPage=' + params.key;
    }

    if (results.length > 0) {
      const firstModuleUrl = results[0];
      const baseUrl = settings.timeoutAppIframes ? 'https://some-url-that-does-not-exist.com' : window.location.origin
      let queryString = 'data-options=' + dataOptions + nextDialogParam + '&theme=' + settings.appTheme;
      if (!isCacheable(params.addon_key, params.key)){
        queryString = queryString + '&jwt=' + unexpiredJWT;
      }
      const url = new RefappUrlBuilder()
        .setBaseUrl(baseUrl)
        .setQueryString(queryString)
        .setModuleUrl(firstModuleUrl)
        .build();
      const resolvedModule = Object.assign(params, {url});
      return resolvedModule;
    } else {
      return null;
    }
  };

  static getContentResolver = settings => {
    return (params) => {
      return new Promise((resolve, reject) => {
        console.log('ACJSRefApp Executing content resolver for params: ', params);
        const resolvedModule = RefappUtil.resolveModule(params, settings);
        if (resolvedModule) {
          resolve(resolvedModule);
          return;
        } else {
          reject(`No content resolver implemented for appKey: ${params.addon_key}, moduleKey: ${params.key}`);
          return;
        }
      });
    };
  }
}
