
const appMinWidth = '800px';
const appMinHeight = '680px';
const acjsRefappName = 'ACJS Refapp';
const jiraProductName = 'Jira';
const confluenceProductName = 'Confluence';

export { appMinHeight, appMinWidth, acjsRefappName, jiraProductName, confluenceProductName };
